package mx.unitec.practica7.ui

import androidx.fragment.app.DialogFragment
import mx.unitec.practica7.model.Contact

interface NoticeDialogListener {
    fun onDialogPositiveClick(dialog: DialogFragment, contacto: Contact)
    fun onDialogNegativeClick(dialog: DialogFragment)
}