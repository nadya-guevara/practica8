package mx.unitec.practica7.ui

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_contact.view.*
import mx.unitec.practica7.R
import mx.unitec.practica7.model.Contact

class ContactDialogFragment : DialogFragment() {

    internal lateinit var listener: NoticeDialogListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {

            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            val view = inflater.inflate(R.layout.dialog_contact, null)
            builder.setTitle("Agregar contacto")
            builder.setView(view)
                .setPositiveButton("Ok",
                    DialogInterface.OnClickListener { dialog, which ->

                        var contacto = Contact(
                            view.txtName.editableText.toString(),
                            view.txtAge.editableText.toString().toInt(),
                            view.txtCorreo.editableText.toString()

                        )

                        listener.onDialogPositiveClick(this, contacto)

                    })
                .setNegativeButton("Cancelar",
                    DialogInterface.OnClickListener { dialog, which ->
                        listener.onDialogNegativeClick(this)

                    })
            builder.create()

        } ?: throw IllegalStateException("La actividad no pede ser vacia")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try{
            listener = context as NoticeDialogListener
        }catch (e: ClassCastException) {
            throw ClassCastException("Debe implementar la interaz correcta")
        }
    }
}