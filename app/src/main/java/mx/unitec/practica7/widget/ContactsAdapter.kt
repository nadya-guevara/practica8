package mx.unitec.practica7.widget

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mx.unitec.practica7.model.Contact

class ContactsAdapter(private var list: MutableList<Contact>) : RecyclerView.Adapter<ContactViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        return ContactViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val contact : Contact = list[position]
        holder.bind(contact)
    }

    override fun getItemCount(): Int {
        return list.size
    }
    fun add(contacto: Contact) {
        list.add(contacto)
        notifyItemInserted(list.size-1)
    }
    
    
}