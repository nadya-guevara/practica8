package mx.unitec.practica7.widget

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.unitec.practica7.R
import mx.unitec.practica7.model.Contact

class ContactViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.item_contact,parent,false)) {

    var txtName : TextView
    var txtAge : TextView
    var txtCorreo : TextView

    init {
        txtName = itemView.findViewById(R.id.txtName)
        txtAge = itemView.findViewById(R.id.txtAge)
        txtCorreo = itemView.findViewById(R.id.txtCorreo)
    }
    fun bind(contact : Contact) {
        txtName.text = contact.name
        txtAge.text = contact.age.toString()
        txtCorreo.text = contact.correo
    }
}