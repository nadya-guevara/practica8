package mx.unitec.practica7.model

data class Contact(var name: String, var age : Int, var correo: String)