package mx.unitec.practica7

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import mx.unitec.practica7.model.Contact
import mx.unitec.practica7.ui.ContactDialogFragment
import mx.unitec.practica7.ui.NoticeDialogListener
import mx.unitec.practica7.widget.ContactsAdapter

class MainActivity : AppCompatActivity(), NoticeDialogListener {

    private var Contacts = mutableListOf(
        Contact("Alan",30,"alan@unitec.mx"),
        Contact("Otro", 35,"otro@unitec.mx"),
        Contact("Daniel",44,"daniel@unitec.mx"),
        Contact("Daniela",22,"daniela@unitec.mx")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        list_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = ContactsAdapter(Contacts)
            addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        }

        fab.setOnClickListener { view ->
            val newfragment = ContactDialogFragment()
            newfragment.show(supportFragmentManager, "Contacto")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDialogPositiveClick(dialog: DialogFragment, contacto: Contact) {
        val adapter = list_recycler_view as ContactsAdapter
        adapter.add(contacto)

        Snackbar.make(findViewById(android.R.id.content), "${contacto.name} agregado", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {
        Snackbar.make(findViewById(android.R.id.content), "Cancelado", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show()
    }
}
